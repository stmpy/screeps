module.exports = function(grunt) {

  var config = require( process.env['SCREEPS_CONFIG'] || './.screeps.json' );

  grunt.initConfig({

    email: grunt.option('email') || process.env['SCREEPS_EMAIL'] || config.email,
    password: grunt.option('password') || process.env['SCREEPS_PASSWORD'] || config.password,
    ptr: grunt.option('ptr') || process.env['SCREEPS_PTR'] || config.ptr,
    branch: grunt.option('branch') || process.env['SCREEPS_BRANCH'] || "<%= gitinfo.local.branch.current.name %>",
    localfolder: grunt.option('localfolder') || process.env['SCREEPS_LOCAL_FOLDER'] || config.localfolder,

    gitinfo: {},
    screeps: {
      options: {
        email: "<%= email %>",
        password: "<%= password %>",
        branch: "master",
        ptr: "<%= ptr %>"
      },
      dist: {
         src: ['dist/*.js']
      }
    },

    // Remove all files from distribution (removes remnants)
    clean: {
      dist: ['dist']
    },

    // Copy source code into dist folder and flatten
    copy: {
      screeps: {
        files: [{
          expand: true,
          cwd: 'src/',
          src: '**',
          dest: 'dist/',
          filter: 'isFile',
          rename: function (dest, src) {
            return dest + src.replace(/\//g,'.');
          }
        }]
      },
      local: {
        files: [{
          expand: true,
          cwd: 'dist/',
          src: '*',
          dest: '<%=localfolder%>',
          filter: 'isFile'
        }]
      }
    },
    rsync: {
      options: {
        args: ["--verbose", "--checksum"],
        exclude: [".git*"],
        recursive: true
      },
      private: {
        options: {
          src: './dist/',
          dest: '<%=localfolder%>'
        }
      }
    }

  });

  grunt.registerTask('build', ['gitinfo', 'clean', 'copy:screeps']);
  grunt.registerTask('public-realm', ['build', 'screeps:dist']);
  grunt.registerTask('local', ['build', 'copy:local']);
  grunt.registerTask('private', ['build', 'rsync:private']);

  grunt.registerTask('default', ['build']);

  grunt.loadNpmTasks('grunt-screeps');
  grunt.loadNpmTasks('grunt-gitinfo');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-rsync');
}
