module.exports = {
  get: function( instance, item, { find, params=[], store = true } ) {
    var item_cache = '_' + item
    var item_ids = item + '_ids'
    // check if the item is not locally stored already
    if( ! instance[ item_cache ] ) {

      // check if key exists
      if( _.isUndefined( instance.memory[item_ids] ) ) {
        instance.memory[item_ids] = {}
      }

      // check if the item ids are stored in memory
      if( store && ! instance.memory[item_ids] || instance.memory[item_ids].length == 0 ) {
        instance.memory[item_ids] = find.apply( instance, params )
                                      .map( _i => _i.id );
        // get the item from the IDs
        instance[item_cache] = instance.memory[item_ids].map( id => Game.getObjectById( id ) );
      } else {
        instance[item_cache] = find.apply( instance, params )
      }

    }

    // return the item
    return instance[item_cache];
  },
  set: function( instance, item, value ) {
    var item_ids = item + '_ids'
    instance.memory[ item_ids ] = value;
  }
}
