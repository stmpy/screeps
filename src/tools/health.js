module.exports = {
  sort: {
    by_least_hits: ( list ) => {
      return _.sortBy( list, 'hits' )
    }
  }
}
