module.exports = {
  sort: {
    by_nearest: function( list, creep ) {
      var sorted_list = _.sortBy( list, ( _i ) => {
        return Math.abs( creep.pos.x - _i.pos.x ) + Math.abs( creep.pos.y - _i.pos.y );
      });
      return sorted_list;
    }
  },
};
