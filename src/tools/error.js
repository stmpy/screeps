module.exports = {
  display: function( code ) {
    // OK: 0,
    // ERR_NOT_OWNER: -1,
    // ERR_NO_PATH: -2,
    // ERR_NAME_EXISTS: -3,
    // ERR_BUSY: -4,
    // ERR_NOT_FOUND: -5,
    // ERR_NOT_ENOUGH_ENERGY: -6,
    // ERR_NOT_ENOUGH_RESOURCES: -6,
    // ERR_INVALID_TARGET: -7,
    // ERR_FULL: -8,
    // ERR_NOT_IN_RANGE: -9,
    // ERR_INVALID_ARGS: -10,
    // ERR_TIRED: -11,
    // ERR_NO_BODYPART: -12,
    // ERR_NOT_ENOUGH_EXTENSIONS: -6,
    // ERR_RCL_NOT_ENOUGH: -14,
    // ERR_GCL_NOT_ENOUGH: -15,
    switch( code ) {
      case ERR_NOT_OWNER: // -1
        console.log( "Error: Not Owner" );
        break;
      case ERR_NO_PATH: // -2
        console.log( "Error: No Path" );
        break;
      case ERR_BUSY: // -4
        console.log( "Error: Resource Busy" );
        break;
      case ERR_INVALID_ARGS:
        console.log( "Error: Invalid Arguments" );
        break;
      default:
        console.log( "Not a known error: " + code );
        break;
    }

  }
}
