var Harvester = require('role.harvester');
var Claimer = require('role.claimer');
var Builder = require('role.builder');
var Upgrader = require('role.upgrader');
var Tower = require('role.tower');
let Error = require('tools.error');
require('init.main');

module.exports.loop = function () {

  // console.log( '===========================================================================' );
  // INITING
  for(var i in Memory.creeps) {
    if(!Game.creeps[i]) {
      console.log('removing: ' + i + ' ... a moment of silence plz' );
      delete Memory.creeps[i];
    }
  }

  for( var _room_name in Game.rooms) {
    var _nextUp = undefined;
    var _room = Game.rooms[_room_name];

    // INITING MEMORY -- MOVE SOMEWHERE ELSE ----------
    if( _.isUndefined( _room.memory.status ) ) {
      _room.memory.status = {};
      if( _.isUndefined( _room.memory.status.sources_refreshed ) ) {
        _room.memory.status.sources_refreshed = false;
      }
    }
    if( _.isUndefined( _room.memory.creeps ) ) {
      _room.memory.creeps = {};

      if( _.isUndefined( _room.memory.creeps.max ) ) {
        _room.memory.creeps.max = 12;
      }
    }

    if( _.isUndefined( _room.memory.resources ) ) {
      var _resources = {};
      var _weight = _room.sources.length / 10;
      _.each( _room.sources, (_s) => _resources[_s.id] = { weight: _weight } )
      _room.memory.resources = _resources;
    }
    // console.log( _.chain( _room.sources ).map( (_s) => { weight: 0.3 } ) )
    // ------------------------------------------

    // when sources refresh then clear targets as well
    var _ttr = _.map( _room.sources, 'ticksToRegeneration' )
    // console.log( 'reset (undefined)? ', _ttr.indexOf( undefined ) )
    // console.log( 'refreshed status: ', Memory.rooms[_room_name].status.sources_refreshed )
    if( _ttr.indexOf( undefined ) >= 0 && Memory.rooms[_room_name].status.sources_refreshed == false ) {
      Memory.rooms[_room_name].status.sources_refreshed = true
      _.forEach( _room.creeps, _c => delete _c.memory.target )
      console.log( '++++++++++++++++++++++ CLEARED THE MEMORY BANKS ++++++++++++++++++++++' )
    }
    else if( _ttr.indexOf( undefined ) == -1 && Memory.rooms[_room_name].status.sources_refreshed == true ) {
      console.log( '++++++++++++++++++++++ WAITING FOR NEXT REFRESH ++++++++++++++++++++++' )
      Memory.rooms[_room_name].status.sources_refreshed = false
    }

    // get some logs and cleanup going
    // console.log( 'room name: ' + _room_name,
    //   'regen: ' + _ttr, '-',
    //   'energy (' + _room.energyAvailable + '/' + _room.energyCapacityAvailable + ')',
    //   '(U: ' + _room.upgraders.length + '/' + Upgrader.qty + ') ' +
    //   '(H: ' + _room.harvesters.length + '/' + Harvester.qty + ') ' +
    //   '(B: ' + _room.builders.length + '/' + Builder.qty + ') ' // +
    //   // 'claimers: (" + Claimer.list.length + "/" + Claimer.qty + ")"
    // );

    // Who's next?
    if( ! _.isUndefined( _room.memory.creeps.max ) && _room.memory.creeps.max <= _room.creeps.length ) { _nextUp = null; }
    else if( _room.harvesters.length < parseInt( Harvester.qty ) ) { _nextUp = Harvester; }
    else if( _room.upgraders.length < parseInt( Upgrader.qty ) ) { _nextUp = Upgrader; }
    else if( _room.builders.length < parseInt( Builder.qty ) ) { _nextUp = Builder; }
    // else if( Claimer.list.length < parseInt( Claimer.qty ) ) { _nextUp = Claimer; }

    if( _nextUp ) {
      var date = new Date()
      var name = _nextUp.name_base.charAt(0) + '-' +
        date.getFullYear() +
        parseInt( date.getMonth() + 1) +
        date.getDate() +
        date.getHours() +
        date.getMinutes() +
        date.getSeconds();

      _.forEach( _room.spawns, function( _spawn ) {
        // var _spawn = _room.spawns[ _spawnName ];
        if( _room.energyAvailable >= 300 ) {

          var new_creep = _spawn.spawnCreep( _nextUp.parts( _room.energyAvailable ), name, {
            memory: _nextUp.default_memory( _room )
          });
          console.log('Spawning new ' + _nextUp.role + ' (' + name + '): ' + new_creep );
          new_creep != OK ? Error.display( new_creep ): "" ;
        }
      });
    }

    _room.towers.forEach( tower => Tower.run( tower ) )
    // Lets run some stuff
    _room.harvesters.forEach( creep => Harvester.run( creep ) )
    // Claimer.list.forEach( creep => Claimer.run( creep ) )
    _room.builders.forEach( creep => Builder.run( creep ) )
    _room.upgraders.forEach( creep => Upgrader.run( creep ) )

    // console.log( '===============' );
  }
}
