/**
 * Find an energy source to collect from
 */

module.exports = {
  name: 'collect',

  available: function( creep, roomName = null ) {
    var room = Game.rooms[ roomName ] || creep.room
    var targets = [];
    // TODO: get this out of here and into something else?
    // targets = creep.room.find( FIND_DROPPED_RESOURCES );
    // console.log( 'targets length: ' + targets.length );
    if( targets.length == 0 )
    {
      var targets = room.find( FIND_SOURCES, {
        filter: (source) => {
          // TODO: locational? the least populated?
          return source.energy > 0
        }
      });
    }
    var ids = _.map( targets, 'id' )

    // if the creep has a target already, and it is valid, then use that
    if( creep.memory.target && _.indexOf( ids, creep.memory.target ) >= 0 ) {
      return Game.getObjectById( creep.memory.target );
    }
    // for now ... just assign the target at random ... ish
    else if( targets.length > 0 ) {

      // generate an array of items for weighted resource movement
      if( typeof Memory.rooms[ room.name ] != undefined ) {
        var min = 0,
          max = 0,
          weighted_list = [];
        for( var _i in targets ) {
          var _t = targets[_i].id;
          // console.log( 'creep room id: ' + creep.room.name );
          // console.log( 'weight: ',Memory.rooms[ creep.room.name ].resources[ _t ].weight * 100 );
          if( Memory.rooms[ room.name ].resources[ _t ] )
          {
            max = parseInt( min ) + parseInt( Memory.rooms[ room.name ].resources[ _t ].weight * 100 );
          }
          else
          {
            max = 50;
          }
          // console.log ('looking @ target: ' + _t + ' min=' + min + ' max=' + max );
          for( var i = min; i < max-1; i++ ) {
            weighted_list.push( _t );
          }
          min = max;
        }
        var _target_id = _.random( max-1 );
        // console.log( creep.name + ': NEW target ==> ' + weighted_list[ _target_id ] );
        return Game.getObjectById( weighted_list[ _target_id ] );
      }
    } else {
      console.log( 'Collection: No target reached' );
      return null
    }
  }
}

