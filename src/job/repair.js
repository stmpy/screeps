var distance = require('tools.distance')
var health = require('tools.health')

module.exports = {
  name: 'repair',

  available: ( _room, _sort = 'health', _find = FIND_MY_STRUCTURES ) => {
    // collect all of the sites that need repaired
    //
    var _sites = _.filter( _room.find( _find ), _s => {
      // hits is less than max hits
      return _s.structureType != STRUCTURE_WALL && _s.hitsMax - _s.hits > 0;
    });

    // Choose a sorting method
    //
    // if( _sort == 'distance' ) {
    //    console.log( 'distance: ' + _sites.length );
    //   _sites = distance.sort.by_nearest( _sites )
    // }

    // else if( _sort == 'health' ) {
      // sort by least amount of health
      // console.log( 'health: ' + _sites.length );
      _sites = health.sort.by_least_hits( _sites )
    // }
    return _sites;
  }
}

