var distance = require('tools.distance');
/**
 * Return a structure awaiting energy transfer
 */
var jobTransfer = {
  available: function( creep, roomName ) {
    var _room = Game.rooms[ roomName ] || creep.room

    var _ext = distance.sort.by_nearest( _room.find( FIND_STRUCTURES, {
      filter: ( _s ) => {
        return _s.structureType == STRUCTURE_EXTENSION
          && _s.energy < _s.energyCapacity;
      }
    }), creep );
    var _tower = distance.sort.by_nearest( _room.find( FIND_STRUCTURES, {
      filter: ( _s ) => {
        return _s.structureType == STRUCTURE_TOWER
          && _s.energy < _s.energyCapacity;
      }
    }), creep );
    var _spawn = distance.sort.by_nearest( _room.find( FIND_STRUCTURES, {
      filter: ( _s ) => {
        return _s.structureType == STRUCTURE_SPAWN
          && _s.energy < _s.energyCapacity;
      }
    }), creep );
    var targets = [].concat( _ext, _tower, _spawn );
    var _ids = _.map( targets, 'id' );
    if( creep.memory.target && _.indexOf( _ids, creep.memory.target ) >= 0 ) {
      return Game.getObjectById( creep.memory.target );
    }
    else if( targets.length > 0 ) {
      // console.log( 'Transfer: NEW target' );
      return _.first( targets );
    } else {
      console.log( '++++++++++++++++++++++++ Transfer: No target reached ++++++++++++++++++++++++' );
      return null
    }
  }
}

module.exports = jobTransfer;
