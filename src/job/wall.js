var distance = require('tools.distance')
var health = require('tools.health')

module.exports = {
  name: 'wall',

  // FIND_MY_STRUCTURES finds:
  // extensions, ramparts, spawn, etc

  // FIND_STRUCTURES finds:
  // everything ... roads, walls, etc ...
  available: ( _creep, _sort = 'health', roomName = null, _find = FIND_STRUCTURES ) => {
    var _room = Game.rooms[ roomName ] || creep.room
    // collect all of the sites that need repaired
    //
    var _sites = _.filter( _room.find( _find ), _s => {
      // hits is less than max hits
      return _s.hitsMax - _s.hits > 0;
    });

    // Choose a sorting method
    //
    if( _sort == 'distance' ) {
      _sites = distance.sort.by_nearest( _sites, _creep )
    }

    else if( _sort == 'health' ) {
      // sort by least amount of health
      _sites = health.sort.by_least_hits( _sites )
    }
    return _sites;
  }
}

