var memory = require('tools.memory');

Object.defineProperty(Room.prototype, 'sources', {
  get: function() {
    return memory.get( this, 'sources', {
      find: function() {
        return this.find(FIND_SOURCES);
      }
    });
  },
  set: function(newValue) {
    // when storing in memory you will want to change the setter
    // to set the memory value as well as the local value
    this.memory.sources = newValue.map(source => source.id);
    this._sources = newValue;
  },
  enumerable: false,
  configurable: true
});

Object.defineProperty( Room.prototype, 'spawns', {
  get: function() {
    return memory.get( this, 'spawns', {
      find: function() {
        return this.find( FIND_MY_SPAWNS );
      }
    });
  }
});

Object.defineProperty( Room.prototype, 'creeps', {
  get: function() {
    return memory.get( this, 'creeps', {
      find: function() {
        var room = this
        return _.filter( Game.creeps, c => c.memory.room == room.name )
      },
      store: false
    });
  },
  set: function( value ) {
    memory.set( this, 'creeps', value );
  }
});

Object.defineProperty( Room.prototype, 'harvesters', {
  get: function() {
    return memory.get( this, 'harvesters', {
      find: function() {
        return _.filter( this.creeps, c => c.memory.role == 'harvester' )
      },
      store: false
    });
  }
});

Object.defineProperty( Room.prototype, 'upgraders', {
  get: function() {
    return memory.get( this, 'upgraders', {
      find: function() {
        return _.filter( this.creeps, c => c.memory.role == 'upgrader' )
      },
      store: false
    });
  }

});

Object.defineProperty( Room.prototype, 'builders', {
  get: function() {
    return memory.get( this, 'builders', {
      find: function() {
        return _.filter( this.creeps, c => c.memory.role == 'builder' )
      },
      store: false
    });
  }
});

Object.defineProperty( Room.prototype, 'towers', {
  get: function() {
    return memory.get( this, 'towers', {
      find: function() {
        return this.find( FIND_MY_STRUCTURES, { filter: _o => _o.structureType == STRUCTURE_TOWER });
      },
      store: true
    });
  }
});

Object.defineProperty( Room.prototype, 'hostiles', {
  get: function() {
    return memory.get( this, 'hostiles', {
      find: function() {
        return this.find( FIND_HOSTILE_CREEPS );
      },
      store: false
    });
  }
});
