var collect = require('job.collect');
var creepRole = require('role.creep');

var role = 'upgrader'
var _creepRole = creepRole( role )
var run = creep => {

  /** @param {Creep} creep **/

  if( _.sum( creep.carry ) == 0 && creep.memory.collecting != true) {
    delete creep.memory.upgrading;
    creep.memory.collecting = true;
    creep.say( 'collecting' );
  }
  else if( _.sum( creep.carry ) == creep.carryCapacity && creep.memory.upgrading != true ) {
    creep.memory.upgrading = true;
    delete creep.memory.collecting;
    creep.say( 'upgrading' );
  }

  if(creep.memory.upgrading) {
    creep.memory.target = creep.room.controller.id;
    if(creep.upgradeController(creep.room.controller) == ERR_NOT_IN_RANGE) {
      creep.moveTo(creep.room.controller);
      creep.memory.message = 'upgrading: ' + creep.room.controller.id;
    }
  }
  else {
    var source = collect.available( creep, creep.memory.room );
    if(creep.harvest(source) == ERR_NOT_IN_RANGE) {
      creep.moveTo(source);
      creep.memory.message = 'harvesting: ' + parseInt( ( creep.carry.energy / creep.carryCapacity ) * 100 ) + '% ' + source.id;
    }
    // blind error
    if( source ) {
      creep.memory.target = source.id;
    }
  }
  _creepRole.post_run( creep )
};

module.exports = _.extend( _creepRole, {
  name_base: "Uppity",
  qty: 4,
  run: run
});
