var transfer = require('job.transfer');
var collect = require('job.collect');

// var model = require('model.harvester');
var creep_role = require('role.creep');

var role = 'harvester';
var _creep_role = creep_role( role )
var run = creep => {
  // Change Mode
  if( _.sum( creep.carry ) == 0 && creep.memory.collecting != true) {
    delete creep.memory.transferring;
    creep.memory.job = collect.name
    creep.memory.collecting = true;
    creep.say( 'collecting' );
  }
  else if( _.sum( creep.carry ) == creep.carryCapacity && creep.memory.transferring != true )
  {
    // creep.say( "Transferring" );
    delete creep.memory.collecting;
    creep.memory.transferring = true;
    creep.say( 'transferring' );
  }

  // Next action
  if( creep.memory.collecting ) {
    var source = collect.available( creep, creep.memory.room );
    if( creep.harvest(source) == ERR_NOT_IN_RANGE) {
        creep.moveTo(source);
    }

    if( source ) {
      creep.memory.target = source.id;
      // console.log( 'harvesting: ' + creep.memory.target );
      creep.memory.message = 'harvesting: ' + creep.memory.target;
    }
  }
  else
  {
    var _target = transfer.available( creep, creep.memory.room );
    if( _target ) {
      var _code = creep.transfer(_target, RESOURCE_ENERGY);
      if( _code == ERR_NOT_IN_RANGE) {
          creep.moveTo(_target);
      }
      else if( _code == ERR_INVALID_TARGET ) {
        delete creep.memory.target;
      }
      creep.memory.target = _target.id;
      // console.log( 'transferring: ' + creep.memory.target );
      var _object = Game.getObjectById( creep.memory.target );
      creep.memory.message = 'transferring: ' + ( _object.name || _object.structureType );
    }

    // Nothing to do ... become a builder
    // else {
    //   creep.memory.role = 'builder';
    //   // need a command to add to builder list
    //   // run that command here
    // }
  }
  _creep_role.post_run( creep )
}

module.exports = _.extend( _creep_role, {
  qty: 3,
  run: run,
  name_base: 'Harvey'
});

