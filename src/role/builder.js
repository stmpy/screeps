var collect = require('job.collect');
var creepRole = require('role.creep');
var distance = require('tools.distance');
var repair = require('job.repair');
var wall = require('job.wall');

var role = 'builder'
var _creepRole = creepRole( role )
var  run = creep => {
  // console.log( creep.room.towers.length > 0 ? "yes": "no" );

  // if( creep.room.name == "W13N3" ) {
  //   creep.memory.moving = true
  // }
  if( creep.room.builders.length > 4 && creep.room.upgraders.length < 2 ) {
    creep.memory.role = 'upgrader';
    // need a command to add to upgrader list
    // run that command here
  }
  if( _.sum( creep.carry ) == 0 && creep.memory.collecting != true) {
    // delete creep.memory.moving
    delete creep.memory.building;
    delete creep.memory.action;
    delete creep.memory.target;
    creep.memory.collecting = true;
  }
  else if( _.sum( creep.carry ) == creep.carryCapacity && creep.memory.building != true) {
    // delete creep.memory.moving
    delete creep.memory.collecting;
    delete creep.memory.action;
    delete creep.memory.target;
    creep.memory.building = true;
  }

  // if(creep.memory.moving) {
  //   creep.moveTo( new RoomPosition( 3,15, "W12N3" ) )
  // }
  if(creep.memory.building) {
    // var constructionSites = construction( creep, 'distance' )
    var constructionSites = distance.sort.by_nearest( creep.room.find(FIND_CONSTRUCTION_SITES), creep );
    var repairSites = repair.available( Game.rooms[ creep.memory.room ], 'health' );
    var wallSites = wall.available( creep, 'health', creep.memory.room );
    var towersPresent = creep.room.towers.length > 0

    //constructionSites = _.sortBy( constructionSites, ( _site ) => {
    //  return Math.abs( creep.pos.x - _site.pos.x ) + Math.abs( creep.pos.y - _site.pos.y );
    //});

    // determin action
    // repairing "FIND_MY_STRUCTURES" is most important, if no tower is present
    // build next
    // then wall repairs
    if( creep.memory.action == 'repair' ) {
      var target = Game.getObjectById( creep.target )
      // console.log( target.hits + " (" + target.hits * 1.10 + ") == " + target.hitsMax );
      if( target && target.hits == target.hitsMax ) {
        // console.log( "CHANGE TARGET" );
        delete creep.memory.target;
      }
    }

    if( creep.memory.target ) {
      // don't need to do anything ... just keep going at it
      if( _.isUndefined( creep.memory.action ) ) {
        creep.memory.action = 'repair';
      }
    }

    // repair my structures first if no tower present
    else if( repairSites.length > 0 && !towersPresent )
    {
      creep.memory.target = _.first( repairSites ).id;
      creep.memory.action = 'repair';
      creep.memory.message = 'repairing';
    }

    // do construction
    else if( constructionSites.length > 0 )
    {
      creep.memory.action = 'build';
      creep.memory.message = 'building';
      creep.memory.target = _.first( constructionSites ).id;
    }

    // repair structures even if towers are present
    else if( repairSites.length > 0 )
    {
      creep.memory.target = _.first( repairSites ).id;
      creep.memory.action = 'repair';
      creep.memory.message = 'repairing';
    }

    // increase wall health
    else if( wallSites.length > 0 )
    {
      creep.memory.target = _.first( wallSites ).id;
      creep.memory.action = 'repair';
      creep.memory.message = 'repairing';
    }
    else
    {
      creep.memory.message = 'DOING NOTHING';
    }

    // console.log( creep.memory.target );

    if( creep.memory.target ) {
      var site = Game.getObjectById( creep.memory.target );
      var code = creep[creep.memory.action]( site );
      if(code == ERR_NOT_IN_RANGE) {
        creep.moveTo( site );
      }

      // if the target is invalid, then remove it ... wait for next cycle
      else if( code == ERR_INVALID_TARGET ) {
        delete creep.memory.target;
        creep.say( "I'm confused" );
        console.log( "builder " + creep.name + " can't figure out what to do" )
      } else if( code != OK ) {
        console.log( "unable to do " + creep.memory.action + " because: " + code );
      }
    }
  }
  else {
    var source = collect.available( creep, creep.memory.room );
    if(creep.harvest(source) == ERR_NOT_IN_RANGE) {
      creep.moveTo(source);
    }

    // TODO: figure out why source is returning null ... instead of just if'ing it here?
    if( source ) {
      creep.memory.target = source.id;
      creep.memory.message = 'harvesting: ' + creep.memory.target;
    }
  }
  _creepRole.post_run( creep )
}


module.exports = _.extend( _creepRole, {
  qty: 4,
  run: run,
  name_base: "Bob"
});
