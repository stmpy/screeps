var creep_role = require('role.creep')
var role = 'claimer'
var _creep_role = creep_role( role )
var run = creep => {
  // Change mode
  // ???

  // Move
  if( creep.memory.moving ) {
    // creep.moveTo( new RoomPosition( 49, 8, "W13N3" ) )
    // creep.moveTo( new RoomPosition( 43, 12, "W12N3" ) )
    creep.moveTo( new RoomPosition( 14, 15, "W12N3" ) )
    // creep.moveTo( creep.room.controller )
  }
  else if( creep.memory.claim ) {
    output = creep.claimController( creep.room.controller );
    console.log( 'claiming', output );
  }
  //else if( creep.memory.attacking ) {
  //  output = creep.
  //}

  _creep_role.post_run( creep )
}
module.exports = _.extend( _creep_role, {
  run: run,
  name_base: 'Proclaimer',
  parts: {
    8: [ TOUGH, TOUGH, ATTACK, CLAIM, MOVE, MOVE ]
  }
});
