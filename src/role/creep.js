module.exports = ( role ) => {
  return {
    role: role,
    name_base: 'Carpy',
    list: {},
    parts: function( availableEnergy ) {
      let bodyParts = [
        {
          "part": WORK,
          "cost": 100,
          "percentage": 1/3
        },
        {
          "part": CARRY,
          "cost": 50,
          "percentage": 1/3
        },
        {
          "part": MOVE,
          "cost": 50,
          "percentage": 1/3
        }
      ];
      let partList = [];
      let price = 0;

      // iterate over body parts and add them accordingly
      _.forEach( bodyParts, function( bodyPart ) {

        // split available eneergy by percentage, then divide by the cost of the part to get the amount of parts
        let amount = ( availableEnergy * bodyPart.percentage ) / bodyPart.cost;
        // console.log( "amount:", amount );

        for( let i = 1; i <= amount; i++ ) {
          partList.push( bodyPart.part );
          price += bodyPart.cost;
        }
      });

      // console.log( "price:", price );
      return partList;
    },

    default_memory: _room => {
      return {
        role: role,
        room: _room.name
      }
    },

    post_run: creep => {
      if( creep.memory.target && Game.getObjectById( creep.memory.target ) ) {
        var _target = Game.getObjectById( creep.memory.target )
        creep.room.visual.line(creep.pos, _target.pos,
            {color: 'gray', lineStyle: 'dashed'});
      }
      // console.log( "===== " + creep.name + ' (' + creep.memory.role + ' - ttl: ' + creep.ticksToLive + ')' + ( creep.memory.message ? ' ==> ' + creep.memory.message : "" ) );
    }
  }
};
