let creep_role = require( 'role.creep' );

let role = 'healer';
let _creep_role = creep_role( role );

let run = creep => {

  _creep_role.post_run( creep )
}

module.exports = _.extend( _creep_role, {
  qty: 1,
  run: run,
  name_base: 'Helio'
});
