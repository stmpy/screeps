var repair = require( 'job.repair' );

Object.defineProperty( StructureTower.prototype, 'memory', {
  get: function() {
    if( _.isUndefined( Memory.towers ) ) {
      Memory.towers = {};
    }
    if(! _.isObject(Memory.towers)) {
      return undefined;
    }
    return Memory.towers[this.id] =
      Memory.towers[this.id] || {};
  },
  set: function() {
    if( _.isUndefined( Memory.towers ) ) {
      Memory.towers = {};
    }
    if( !_.isObject( Memory.towers ) ) {
      throw new Error( 'Cound not set source memory' );
    }
    Memory.towers[this.id] = value;
  }
});

var run = tower => {
  // should move to room prototype
  var hurt_guys = _.filter( tower.room.creeps, creep => creep.hits < creep.hitsMax )
  var needs_repair = repair.available( tower.room, 'distance' );
  // console.log( "needs_repair: " + needs_repair.length );
  // cleanup
  if( ( tower.memory.action == 'attack' && tower.room.hostiles.length == 0 ) ||
    ( tower.memory.action == 'heal' && hurt_guys.length == 0 ) ||
    ( tower.memory.action == 'repair' && needs_repair == 0 ) ) {
    delete tower.memory.action;
    delete tower.memory.target;
  }

  // Order of operations
  // attack -- if ever there are hostiles then immediately attack
  if( tower.memory.target ) {
    // don't do anything ... already doing something
  }
  else if( tower.room.hostiles.length > 0 ) {
    tower.memory.action = 'attack';
    tower.memory.target = _.first( tower.room.hostiles ).id;
  }
  // heal
  else if( hurt_guys.length > 0 ) {
    // console.log( 'hurt dudes: ' + hurt_guys.length );
    tower.memory.action = 'heal';
    tower.memory.target = _.first( hurt_guys ).id;
  }
  // repair
  else if( needs_repair.length > 0 ) {
    tower.memory.action = 'repair';
    tower.memory.target = _.first( needs_repair ).id;
  }


  // execute action
  if( tower.memory.action ) {
    // console.log( "Tower doing: " + tower.memory.action + " to " + tower.memory.target );
    code = tower[tower.memory.action]( Game.getObjectById( tower.memory.target ) );

    switch( code ) {
      case OK:
        // all good
        break;
      case ERR_NOT_ENOUGH_RESOURCES:
        // console.log( "Tower does not have sufficient resources" );
        break;
      case ERR_INVALID_TARGET:
        console.log( "Tower target is invalid: " + tower.memory.target );
        delete tower.memory.target;
        break;
      case ERR_RCL_NOT_ENOUGH:
        console.log( "Room Controller is too low to use this structure" );
        break;
      default:
        console.log( "Tower error (" + tower.memory.action + "): " + code );
        break;
    }
  }
}

module.exports = {
  run: run
}
