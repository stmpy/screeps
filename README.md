# Stmpy Screeps

## Setup

copy `.screeps.json.template` and fill out the details according to your account.
If using this repo the `localfolder` will be `/app`.

```bash
# build docker image
./bin/build

# install local grunt
./bin/npm install grunt

# test grunt upload
./bin/grunt
# Sample Output:
# Running "gitinfo" task
#
# Running "clean:dist" (clean) task
# >> 0 paths cleaned.
#
# Running "copy:screeps" (copy) task
# Copied 23 files
#
# Done.
```

## Bin Commands

### Build
`./bin/build` - build screeps docker image for development (used in `./bin/exec` and others)

### Exec
`./bin/exec [COMMAND]`  - execute into docker container of screeps build image (useful for debugging issues)

### Grunt
`./bin/grunt [COMMAND]` - run `npm grunt` command w/ anything passed to it (e.g. `./bin/grunt local`)

#### Available Commands
- `default` or `[none]`: runs the build command
- `build`: cleans the `./dist` folder and builds `*.js` files, then copies them to `./dist`
- `local`: runs `default` then copies files to `localfolder`
- `public-realm`: runs build then copies files to screeps public realm via API


### NPM
`./bin/npm [COMMAND]`   - run `npm` command w/ anything passed to it, (e.g. `./bin/npm install`)
